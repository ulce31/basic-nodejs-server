# Basic NodeJs Server
## Summary
- Task: Set up a simple Node.js server using Express.js.
- Key Concepts: Node.js, Express.js, server setup, routing.
- Explanation: Start with the basics by creating a server that listens on a port and serves a "Hello, World!" message.
## Requirements
- Having NodeJS (13.2.0 or a newer version - "type": "module" in package.json)
- Having [yarn](https://yarnpkg.com/getting-started/install)
- Having a requesting tool ([curL](https://curl.se/), [Postman](https://www.postman.com/)...)
## Make it work 👨‍🔬
- [From GitLab] Clone the project
- [In your shell, in the project folder] ⬇️
```sh 
  yarn install
  node server.js
```
